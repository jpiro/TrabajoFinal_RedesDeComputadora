# shut down the entire infraestructure
docker-compose -f docker-compose.yml down


rm /etc/resolv.conf
mv /etc/resolv-chakup /etc/resolv.conf

# if the server is ubuntu uncomment the follow line (enable local dns ubuntu)
sudo systemctl enable systemd-resolved.service
sudo service systemd-resolved start
sudo service network-manager restart